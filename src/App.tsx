import Incrementer from './components/Incrementer';

// Because the Incrementer component is so small with few nesting levels, 
// did I not see a need to use the Context API. It might even make the 
// component harder to resue.

function App() {
  return (
    <div>
      <Incrementer size={'small'} />
      <Incrementer size={'medium'} max={10}/>
      <Incrementer disabled={true} size={'large'} min={0} />
    </div>
  );
}

export default App;
