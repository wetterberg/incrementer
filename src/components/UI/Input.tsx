import React, { ChangeEvent } from 'react';
import styles from './Input.module.css';

type InputProps = {
  value: string;
  disabled?: boolean;
  ref: HTMLInputElement;
  size: 'small' | 'medium' | 'large';
  onChange: (event: ChangeEvent<HTMLInputElement>) => void;
  onBlur: () => void;
}

const Input = React.forwardRef<HTMLInputElement, InputProps>((props, ref) => {
  const classes: string = styles.incrementInput + ' ' + styles[props.size];

  return (
    <input
      type="number"
      disabled={props.disabled}
      className={classes}
      value={props.value}
      onChange={props.onChange}
      onBlur={props.onBlur}
      ref={ref}
    />
  );
});

export default Input;
