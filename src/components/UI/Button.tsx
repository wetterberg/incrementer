import { ReactNode } from 'react';
import style from './Button.module.css';

type ButtonProps = {
  onClick: () => void;
  disabled?: boolean;
  children?: ReactNode;
};

const Button = ({ disabled = false, ...props }: ButtonProps) => {
  return (
    <button className={style.button} onClick={props.onClick} disabled={disabled}>
      {props.children}
    </button>
  );
};

export default Button;
