import { useState, useRef, ChangeEvent } from 'react';
import { HiOutlinePlus, HiOutlineMinus } from 'react-icons/hi';
import Button from './UI/Button';
import Input from './UI/Input';
import styles from './Incrementer.module.css';

// The Incrementer component accepts props for size, disabled (optional), and min/max values (optional).
// The min/max values makes it possible to set a predefined set of numerical values to step within.

type IncrementerProps = {
  size: 'small' | 'medium' | 'large';
  disabled?: boolean;
  min?: number;
  max?: number;
};

const Incrementer = ({
  size,
  disabled = false,
  min = 1,
  max = 1000,
}: IncrementerProps) => {
  const [enteredValue, setEnteredValue] = useState<string>(String(min));
  const inputRef = useRef<HTMLInputElement>(null);

  // Adds or removes a value on button click
  const buttonClickHandler = (val: number) => {
    if (inputRef.current) {
      inputRef.current.focus();
    }
    setEnteredValue((prevValue) => {
      let newValue = +prevValue + val;
      if (newValue < min || newValue > max) {
        return prevValue;
      }
      return String(newValue);
    });
  }

  // Sets a new value on typing input
  const incrementerInputChangeHandler = (
    event: ChangeEvent<HTMLInputElement>
  ) => {
    setEnteredValue(event.target.value);
  };

  // Checks if typing input is within range of min/max values on input blur
  const incrementInputBlurHandler = () => {
    let valueNumber = +enteredValue;
    if (valueNumber < min) {
      setEnteredValue(String(min));
    } else if (valueNumber > max) {
      setEnteredValue(String(max));
    } else {
      setEnteredValue(enteredValue);
    }
  };

  return (
    <div className={styles.incrementer}>
      <Button disabled={disabled} onClick={buttonClickHandler.bind(null, -1)}>
        <HiOutlineMinus />
      </Button>
      <Input
        disabled={disabled}
        value={enteredValue}
        onChange={incrementerInputChangeHandler}
        onBlur={incrementInputBlurHandler}
        size={size}
        ref={inputRef}
      />
      <Button disabled={disabled} onClick={buttonClickHandler.bind(null, 1)}>
        <HiOutlinePlus />
      </Button>
    </div>
  );
};

export default Incrementer;
